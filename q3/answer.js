var lineread = require('../lineread'),
    moment = require('moment');

//Line Object Literal
var line = {
  //this function splits each line into an array and then join the string again
  getMonthsWithFiveSundays: function(lineValue){
    //split all the line string on blank space and create the two months
    var splitLine = lineValue.split(' '),
        firstMonth = splitLine[0] + ' '+ splitLine[1],
        secondMonth = splitLine[2] + ' '+ splitLine[3],
        dateFormat = "MMM YYYY";

    //Check if dates formats are valid
    if(!(moment(firstMonth, dateFormat).isValid() && moment(secondMonth, dateFormat).isValid())){

       console.log('input.txt example content: September 2013 March 2014');
       process.exit(-1);
    }

    //Calculate first and second month and the difference between them
    var firstMonth = moment(firstMonth, dateFormat),
        secondMonth = moment(secondMonth, dateFormat),
        monthsDifference = secondMonth.diff(firstMonth, 'months', true);

    //Check if first month is before the second
    if(monthsDifference<0){
      console.log('The first month should be before the second month');
      process.exit(-1);
    }

    var calculatedMonth = firstMonth,
        monthsCounter = 0;

    //We include the last month so we need to add the last month
    if(monthsDifference > 0){
      monthsDifference += 1;
    }

    while(monthsDifference > 0){
      if(this.getSundaysOfMonth(calculatedMonth) === 5){
        monthsCounter++;
      };
      //Add one month
      calculatedMonth = moment(calculatedMonth).set('month',moment(calculatedMonth).month()+1);

      monthsDifference--;
    }

    console.log(monthsCounter);
  },
  //function to calculate Sundays of the month
  getSundaysOfMonth: function(date) {
    var date = new Date(date),
        month = date.getMonth(),
        sundaysCount = 0;

    //Get the first Sunday of the month
    while (date.getDay() !== 0) {
        date.setDate(date.getDate() + 1);
    }

    // Increment sundays counter for the current month
    while (date.getMonth() === month) {
        date.setDate(date.getDate() + 7);
        sundaysCount++;
    }

    return sundaysCount;
  }
}

lineread().on('line', function(lineValue){line.getMonthsWithFiveSundays(lineValue)});
