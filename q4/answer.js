var lineread = require('../lineread'),
    Combinatorics = require('js-combinatorics');

//Line Object Literal
var line = {
  //this function splits each line into an array and then join the string again
  displayIntegerPower: function(line){

    //split all the line string on blank
    var splitLine = line.split(' ');


    if(parseInt(splitLine[0]) && parseInt(splitLine[1])){
      console.log(this.calculateIntegerPower(splitLine));
    }
    else{
      console.log('Example Input Usage: 2 4');
      process.exit(-1);
    }
  },
  //this function returns the number of distinct values of x^y
  calculateIntegerPower:function(values) {

    var calculatedValues = [],
        possibleValues = [];

    if(values[0] === values[1]){
      return 1;
    }
    else if(values[0] > values[1]){
      values.unshift(values[1]);
      values.pop();
    }

    var lower = values[0],
        upper = values[1];

    //Create Array with all numbers from lower to upper bound
    for(lower;lower<=upper;lower++){
      possibleValues.push(parseInt(lower));
    }

    //Array that has all possible combinations
    var allCombinationArray = Combinatorics.baseN(possibleValues, 2).toArray();

    for(var index = 1;index <= allCombinationArray.length;index++){
      //Calculate the power of two numbers
      var calculatedPower = Math.pow(allCombinationArray[index-1][0],allCombinationArray[index-1][1]);

      //Check if calculated value already exists in the table
      if (calculatedValues.indexOf(calculatedPower) === -1){
        calculatedValues.push(calculatedPower);
      }
    }

    return calculatedValues.length;

  }
}

lineread().on('line', function(lineValue){line.displayIntegerPower(lineValue)});
