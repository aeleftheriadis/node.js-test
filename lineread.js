//Export lineread module in order to use it on all answer files
module.exports = function(){
  var fs = require('fs'),
      readline = require('readline');

  //Check if user has provided the correct arguments
  if (process.argv.length <= 2) {
      console.log("Example Usage: node q1/answer.js input.txt");
      process.exit(-1);
  }

  //Assign second argument to filestream variable
  var fileStream = process.argv[2];

  //Check if input.txt exists
  fs.exists(fileStream, function(exists) {
    if (!exists) {
      console.log("Please add input.txt")
      process.exit(-1);
    }
  });

  //Check if file has empty content
  fs.readFile(fileStream, function (err, data) {
    if (err) throw err;
    if(data.length === 0){
      console.log("Your file is empty");
      process.exit(-1);
    }
  });

  //Return readline instance
  return readline.createInterface({
      input: fs.createReadStream(fileStream),
      output: process.stdout,
      terminal: false
  });
};
