var lineread = require('../lineread');

//Line Object Literal
var line = {
  //this function splits each line into an array and then join the string again
  reverseLocal: function(lineValue){
    console.log(lineValue.split(" ").reverse().join(" "));
  }
}

lineread().on('line', function(lineValue){line.reverseLocal(lineValue)});
