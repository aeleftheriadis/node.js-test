var lineread = require('../lineread');

//Line Object Literal
var line = {
  denominations:[1,2,5,10,20,50,100,200],
  //this function displays all the distinct coin combinations
  displayCoinsCombinations: function(coin){
    if(parseInt(coin)){
      console.log(this.calculateCoinsCombinations(parseInt(coin),this.denominations));
    }
    else{
      console.log('Add a non zero value');
      process.exit(-1);
    }
  },
  //this function returns the number of ways to calculate denominations of a given target
  calculateCoinsCombinations:function(target, denominations) {
  	var targetInitValue = target;
  	target = [1];

  	for (var denominationsIndex = 0; denominationsIndex < denominations.length; denominationsIndex ++) {
  		for (var targetIndex = 1; targetIndex <= targetInitValue; targetIndex ++) {

  			// initialise current target element of the target array
  			target[targetIndex] = target[targetIndex] ? target[targetIndex] : 0;

  			// Sum of target and number of ways to calculate the target
  			if(targetIndex >= denominations[denominationsIndex]){
          target[targetIndex] += target[targetIndex - denominations[denominationsIndex]];
        }
  		}
  	}
  	return target[targetInitValue];
  }
}

lineread().on('line', function(lineValue){line.displayCoinsCombinations(lineValue)});
